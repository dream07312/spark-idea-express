$('.editable').summernote({
    placeholder: 'แผนของไอเดียของคุณ ใส่ได้เต็มที่ !',
    tabsize: 2,
    height: 300
});

$('.tags').tagsInput({
    'height': '50px',
    'width': 'auto',
});

$("#register_form").validate({
    rules: {
        full_name: "required",
        password: "required",
        confirm_password: {
            required: true,
            equalTo: "#password"
        },
        email: "required",
        confirm_rule: {
            required: true,
        }
    },
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        // Add `has-feedback` class to the parent div.form-group
        // in order to add icons to inputs
        element.parents(".col-sm-5").addClass("has-feedback");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }

        if (!element.next("span")[0]) {
            // { { !--$("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element); --} }
        }
    },
    messages: {
        full_name: "Please Enter your full name.",
        email: "Please Enter your email.",
    },
    debug: true,
    submitHandler: (form) => {
        form.submit();
    }
});


$("#login_form").validate({
    rules: {
        password: "required",
        email: "required",
    },
    errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        // Add `has-feedback` class to the parent div.form-group
        // in order to add icons to inputs
        element.parents(".col-sm-5").addClass("has-feedback");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }


        if (!element.next("span")[0]) {
            // { { !--$("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element); --} }
        }


    },
    messages: {
        email: "Please Enter your email.",
        password: "Please Enter your password.",
    },
    debug: true,
    submitHandler: (form) => {
        form.submit();
    }
});

$('#img-edit').click(() => {
    $('#img-have').hide();
    $('#img-not-have').show();
})

$('#blog-edit').submit(function (e) {
    e.preventDefault();
    console.log('blog-edit :: ')
    const _id = $('#_id').val();
    const title = $('#title').val();
    const body = $('#body').val();
    const tags = $('#tags').val();
    const status = $('#status').val();
    const imgCover = $('#imgCover');

    var formData = new FormData();
    formData.append("imgCover", imgCover[0].files[0]);
    formData.append("_id", _id);
    formData.append("title", title);
    formData.append("body", body);
    formData.append("tags", tags);
    formData.append("status", status);

    for (var p of formData) {
        console.log(p);
    }


    $.ajax({
        url: 'http://localhost:3000/blogs/edit',
        crossDomain: true,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
    }).done(function (data) {
        console.log('ajax ::: ', data)
        console.log(data)
    }).fail(function (err) {
        console.log(err)
    })

}); // end blog edit
