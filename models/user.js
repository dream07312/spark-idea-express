const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;


var userSchema = new Schema({
    // username: {
    //     type: String,
    //     required: true,
    //     trim: true,
    //     minlength: 1
    // },
    password : {
        type: String,
        required: true,
        trim: true,
        minlength: 1        
    },
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1       
    },
    displayname : {
        type: String,
        required: true,
        trim: true,
        minlength: 1  
    }
})

const User = mongoose.model('User', userSchema);
module.exports = { User };