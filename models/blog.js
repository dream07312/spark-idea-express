const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;


var blogSchema = new Schema({
    title: {
       type : String,
       required : true,
       trim : true,
       minlength : 1
    },
    author: {
        type :  ObjectId,
        ref: 'User'
        // default: ObjectId("5a3e83c55ff272ba6c8cc87f"),
    },
    user : {
        type: ObjectId,
        ref: 'User'
    },
    body: {
       type : String,
       required : true,
       trim : true,
       minlength : 1
    },
    imgCover : {
        type : String,
        trim : true,
    },
    imgCard : {
        type : String,
        trim : true,
    },
    comments: [
        {
            body:  {
                type : String,
                required : true,
                trim : true,
                minlength : 1
            },
            date: Date,
            user : ObjectId,
        }
    ],
    createDate: {
        type: Date,
        default: Date.now
    },
    editDate : {
        type: Date,
        default: Date.now
    },
    status :{
        type : String,
        enum : ['draft' , 'post'],
        default: 'draft'
    },
    meta: {
        likes: {
            type : Number,
            default : 0
        },
        views : {
            type : Number,
            default : 0
        },
        tags: [  ]
    }
})

const Blog = mongoose.model('Blog', blogSchema);
module.exports = { Blog };