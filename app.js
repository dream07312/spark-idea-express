var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
const hbs = require('hbs');
const moment  = require('moment')
const expressValidator = require('express-validator');
const expressSession = require('express-session');

var mongoose = require('./mongoose');
var index = require('./routes/index');
var users = require('./routes/users');
var blogs = require('./routes/blogs');

var app = express();
const session = { };



// view engine setup
app.set('views', path.join(__dirname, 'views'));
hbs.registerPartials(__dirname + '/views/partials');

hbs.registerHelper('json', function (context) {
  return JSON.stringify(context);
});

hbs.registerHelper('moment', function (context, block) {
  return moment(context).format("DD/MMM/YY"); 
});


app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressValidator());
// set session 
app.use(expressSession({ secret: 'spark_ideal', saveUninitialized: true, resave: false }));





app.use(function (req, res, next) {

  res.locals.myVar  = 'not found'

  console.log('check Session :::' , req.url);
  console.log( req.session );

  console.log('--------')
  if(req.url === '/blogs/create'){
    if (!req.session.authenticated ) {
        res.redirect('/')
    }else{
      next();
    }
  }else{
    next();
  }
})

// function checkAuth(req, res, next) {

//   // console.log('checkAuth ' + req.url);
//   // don't serve /secure to those not logged in
//   // you should add to this list, for each and every secure url
//   if (req.url === '/secure' && (!req.session || !req.session.authenticated)) {
//     console.log('false ................')
//     next();
//     // res.render('unauthorised', { status: 403 });
//   }else{
//     console.log('have session')
//     next();
//   }
// }

// app.use(checkAuth)


app.use('/', index);
app.use('/users', users);
app.use('/blogs', blogs);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});





// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
