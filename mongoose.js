const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const URL = `mongodb://localhost:27017/sparkdb`;
const connection = mongoose.connect(URL , { useMongoClient : true});

connection.on('error', console.error.bind(console , 'Unable Connection to Mongodb server.'));
connection.once('open' , () => {
    console.log('Connected Mongodb server.');
})

module.exports  =  { mongoose };

