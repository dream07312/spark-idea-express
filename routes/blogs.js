var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var { Blog } = require('../models/blog');
const fs = require('fs');
const path = require('path');
const multer = require('multer')
var bodyParser = require('body-parser')
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })



const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const PATH_UPLOAD = `./public/uploads/temp/${req.session.user._id}`
        if (fs.existsSync(PATH_UPLOAD)) {
            cb(null, `${PATH_UPLOAD}`)
        } else {
            fs.mkdirSync(PATH_UPLOAD);
            cb(null, `${PATH_UPLOAD}`)
        }
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

var upload = multer({ storage: storage }).single('imgCover')


function checkSession(req, res) {
    // console.log('checkSession req ,res' + req.url);
    if (!req.session.user || !req.session.authenticated) {
        // console.log('checkSession req ,resfalse')
        return res.send('Please Login');
    } else {
        // console.log('checkSession req ,restrue :: ' , req.session.user);
        // console.log('---------------------------------------')
        return true;
    }

}



/* GET users listing. */
router.get('/', (req, res, next) => {
    res.locals.user = req.session.user || null;
    Blog.find().select({ "_id": 1, "title": 1, "createDate": 1 }).then(posts => {
        res.render('home', {
            title: 'home',
            posts: posts,
        })
    })
});

router.post('/edit', urlencodedParser, (req, res, next) => {
    res.locals.user = req.session.user || null;
    upload(req, res, function (err) {
        if (err) {
            res.json({ msg: err });
        } else {
            const { _id, title, body, tags, status } = req.body;
            arrayTags = tags.split(",");
            let meta = {},
                imgCover
            meta.tags = arrayTags;
            if (req.file) {
                imgCover = `/uploads/temp/${req.session.user._id}/${req.file.filename}`;
            }
            Blog.findOneAndUpdate({ _id: _id }, { title, body, meta, status, imgCover }, { $new: true }).then(data => {
                res.json({ status: true, data: data })
            }).catch(err => res.json({ msg: 'Not save record data.' }))
        }
    })
})


router.post('/', (req, res, next) => {
    res.locals.user = req.session.user || null;
    upload(req, res, function (err) {
        if (err) {
            // console.log('upload images err ::  ', err)
            res.json({ msg: err });
        } else {
            if (req.file === undefined) {
                res.json({ msg: 'images undefined' });
            } else {
                const { title, body, tags, status } = req.body;
                if (!title || !body) return res.json({ msg: 'Not have data for record.' })
                //cover array tag for meta fields.
                arrayTags = tags.split(",");
                let meta = {};
                let imgCover = `/uploads/temp/${req.session.user._id}/${req.file.filename}`;
                // console.log(imgCover)
                meta.tags = arrayTags;
                let newBlog = new Blog({
                    title, body, imgCover, meta, status, user : req.session.user._id,
                    author: req.session.user._id // mock author
                })
                newBlog.save().then(response => {
                    res.status(200).json({
                        msg: response,
                        status: "success"
                    })
                }).catch(err => res.json({ msg: 'Not save record data.' }))
            }
        }



    })

});

router.get('/create', (req, res, next) => {
    res.locals.user = req.session.user || null;
    res.render('blog-create', {
    })
});


router.get('/:id', (req, res, next) => {
    console.log('get:id :: ' ,  req.body)
    res.locals.user = req.session.user || null;
    const id = req.params.id;
    const a = Blog.findById(id).populate({ path: 'author' }).exec()
    
    a.then( blog => {
        console.log('blog :: ', blog)
        return res.render('blog', { post: blog , session: req.session.user || false })

    })

    // .exec(function(err , result){
    //     console.log('err :: ' , err)
    //     console.log('result :: ' , result);
    //     return res.render('blog', { post : result , session: req.session.user || false })
    // })
    //     .catch(err => res.render('Not Found.', { session: req.session.user || false }))
});


router.get('/:id/edit', (req, res, next) => {
    const id = req.params.id;
    res.locals.user = req.session.user || null;
    Blog.findById(id).then(post => {
        // if(req.session.user._id == post.author){
        res.render('blog-edit', { post })
        // }
    }).catch(err => {
        res.json({ err })
    })
})



module.exports = router;
