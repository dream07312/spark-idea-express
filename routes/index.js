var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var { Blog } = require('../models/blog');

/* GET home page. */
router.get('/', function(req, res, next) {
  Blog.find().select({ "_id": 1, "title": 1, "createDate": 1 }).then(posts => {
  	res.locals.user = req.session.user || null ;
  	console.log('req.locals ::: ' ,  req.locals)
  	console.log('res.locals ::: ' ,  res.locals)
  	console.log('req.session :: ' , req.session)
  	console.log('---------')
    res.render('home', {
      title: 'home',
      posts: posts,
      // msgError : req.session.msgError
    })
  })
});


router.get('/test', function(req, res, next) {
  	res.locals.user = req.session.user || null ;
 	res.json({ status : true })
});


// router.get('/formtest', function (req, res, next) {
//   res.render('formtest' , { title : 'form validation' , success : req.session.success , errors: req.session.errors});
//   req.session.errors = null;
// });


// router.post('/submit' , (req ,res ,next) => {
//   console.log('submit')
//   console.log(req.body);
//   console.log('-----------------')
//     req.check('emailTest' , 'Invalid email address').isEmail();
//     req.check('passwordTest' , 'Password is invalid').isLength({min : 4}).equals(req.body.confirmPasswordTest)
//     var errors = req.validationErrors();
//     console.log('errors :: ', errors)
//     if(errors){
//       req.session.errors = errors;
//       req.session.success = false;
//     }else{
//       req.session.success = true;
    
//     }
//     res.redirect('/formtest')
  

// })





module.exports = router;
