var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var { User } = require('../models/user');
var { Blog } = require('../models/blog');

// const genToken = (user) => {
//   return jwt.sign({ sub : user.id }, 'secret-spark' ,  {expiresIn : '1h'});
// }





function checkSession(req, res) {
  // console.log('checkSession req ,res' + req.url);
  if (!req.session.user || !req.session.authenticated) {
    console.log('checkSession req ,resfalse')
    return res.send('Please Login');
  } else {

    console.log('checkSession req ,restrue :: ', req.session.user);
    console.log('---------------------------------------')
    return true;
  }

}


router.post('/login' , (req ,res , next) => {
  const  {  email_login : email  , password_login : password } = req.body;

  User.findOne({
    email
  }).then( user => {
    console.log('result :: ' , user)
      if (user === null) return res.status(404).json({ msg: 'Not fonud account !' });
      const hash = user.password;
      bcrypt.compare(password, hash, (err, isValid) => {
        if (!isValid) return res.status(404).json({ msg: 'Password not valid !' });
        console.log('user -----> ' , user)
        req.session.user = user;
        req.session.authenticated = true;
        res.locals.user = req.session.user || null ;

        console.log(res.locals)
        console.log('---- end res.locals -----')
        return res.redirect('/')
      })
    }).catch(err => res.status(404).json({ msg: 'Not fonud account !' }) )
})


router.post('/register', (req, res, next) => {
  const { email, full_name , password  , confirm_password } = req.body;
  // check emai.
  User.findOne({ email }).then( email_valid => {
    if (email_valid){
      req.session.msgError = true;
      return res.redirect('/');
    }
    // compare passwrd with hashpassword
    bcrypt.hash(password , 13 , (err , hash) => {
      if (err) return res.status(404).json({ msg: err });
      let newUser = new User({ email, password: hash, displayname: full_name })    
      newUser.save().then(response => {
        req.session.msgError = false;
        res.locals.user = req.session.user || null ;
        res.
          // header('Authorization' , `Bearer ${genToken(response.username)}`).
          status(200)
          .json({
          msg: response,
          status: "success"
        })
      }).catch( err => res.status(404).json({ msg : err }))
    })

  })
 
});

router.get('/:id/bloglist',  (req, res , next) => {
  console.log('id ::: ' , req.params.id)
    const id = req.params.id;
    res.locals.user = req.session.user || null ;
    
    Blog.find({ author : id }).select({ "_id" : 1 , "title" : 1 , "createDate" : 1}).then( posts => {
      console.log(posts)
        res.render('user/blog-list', {
             title : 'blog-list',
             posts : posts,
        })        
    })
})


module.exports = router;
